## Title

BookMyTicket - React.JS project
A ticket booking app

## Installation Steps

## 1) Clone the repo

git clone git@gitlab.com:jaydeep.pathak/bookmyticketreact.git

## 2) Install Dependencies

npm install

## 3) Run The App

npm start

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
