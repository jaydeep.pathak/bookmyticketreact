import React, { Component } from "react";
import { Modal, Form, Button } from "react-bootstrap";

class UserDetails extends Component {
  constructor(props) {
    super(props);
    const { selectedSeat, name, email, modalShow } = props;
    console.log(props);
    this.state = {
      selectedSeat,
      name,
      email,
      modalShow
    };
  }

  componentWillReceiveProps(newProps) {
    const oldProps = this.props;
    if (oldProps.modalShow !== newProps.modalShow)
      this.setState({ modalShow: newProps.modalShow });
    if (oldProps.name !== newProps.name) this.setState({ name: newProps.name });
    if (oldProps.email !== newProps.email)
      this.setState({ email: newProps.email });
    if (oldProps.selectedSeat !== newProps.selectedSeat)
      this.setState({ selectedSeat: newProps.selectedSeat });
  }

  handleModalClose = e => {
    this.props.handleModalClose(e);
  };

  render() {
    return (
      <Modal
        show={this.state.modalShow}
        onHide={e => {
          this.props.handleModalClose(e);
        }}
      >
        <Modal.Header>
          <Modal.Title>
            Just One Step Away from you tickets,
            <Form.Text className="text-muted">
              Please Give Your Details <br /> We'll never share your email with
              anyone else.
            </Form.Text>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            onSubmit={e => {
              this.props.handleBookingSubmit(e);
            }}
          >
            <Form.Group>
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                name="name"
                value={this.state.name}
                onChange={e => {
                  this.props.handleChange(e);
                }}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email"
                name="email"
                value={this.state.email}
                onChange={e => {
                  this.props.handleChange(e);
                }}
              />
            </Form.Group>
            <Button variant="primary" type="submit">
              Submit
            </Button>
            <Form.Text className="text-muted">
              {`Your selected seat is ${this.state.selectedSeat}`}
            </Form.Text>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export default UserDetails;
