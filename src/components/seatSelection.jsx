import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import "./seatSelection.css";

class SeatSelection extends Component {
  constructor(props) {
    super(props);
    const { seats } = props;
    this.state = {
      seats
    };
  }
  bookingId = localStorage.getItem("bookingId");
  seatName = localStorage.getItem("seatName");
  render() {
    const { seats } = this.state;
    return (
      <React.Fragment>
        <div className="container mainDiv">
          <p className="screenDirection">All eyes this way please!</p>
          <div style={{ textAlign: "center", marginTop: "40px" }}>
            {seats.slice(0, 10).map((seat, index) => (
              <span
                key={index}
                id={seat.seatId}
                className={seat.status}
                onClick={e => {
                  this.props.handleSeatSelect(e);
                }}
              >
                {seat.seatName}
              </span>
            ))}
            <br />
            <br />
            <br />
            {seats.slice(10, 20).map((seat, index) => (
              <span
                key={seat.seatName}
                id={seat.seatId}
                className={seat.status}
                onClick={e => {
                  this.props.handleSeatSelect(e);
                }}
              >
                {seat.seatName}
              </span>
            ))}
            <br />
            <br />
            <br />
            {seats.slice(20, 30).map((seat, index) => (
              <span
                key={seat.seatName}
                id={seat.seatId}
                className={seat.status}
                onClick={e => {
                  this.props.handleSeatSelect(e);
                }}
              >
                {seat.seatName}
              </span>
            ))}
          </div>
          <div>
            <button
              className="bookTicket"
              onClick={e => {
                this.props.handleBookTicket(e);
              }}
            >
              Book Ticket
            </button>
            {this.bookingId && (
              <button
                className="unBookTicket"
                onClick={e => {
                  this.props.handleUnBookTicket(e);
                }}
              >
                UnBook Ticket
              </button>
            )}
            {this.seatName && (
              <Form.Text className="text-muted">
                {`You have booked Seat No: ${this.seatName}`}
              </Form.Text>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default SeatSelection;
