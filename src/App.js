import React, { Component } from "react";
import SeatSelection from "./components/seatSelection";
import UserDetails from "./components/userDetails";
import axios from "axios";
import "./App.css";

const apiEndpoint = "http://bookmytickettest.000webhostapp.com/api.php";

class App extends Component {
  state = {
    selectedSeats: 0,
    selectedSeat: "",
    selectedSeatId: "",
    name: "",
    email: "",
    modalShow: false
  };

  async componentDidMount() {
    let formData = new FormData();
    formData.append("action", "listAllSeats");
    const { data } = await axios.post(apiEndpoint, formData);
    this.setState({ seats: data.data });
  }

  handleSeatSelect = e => {
    e.preventDefault();
    const { selectedSeats, seats } = this.state;
    const id = e.target.id;
    const seatIndex = seats.findIndex(function(seat) {
      return seat.seatId === id;
    });

    if (seats[seatIndex].status === "available") {
      if (selectedSeats >= 1) alert("You have already selected a seat !");
      else {
        const updatedSeats = [...seats];
        updatedSeats[seatIndex].status = "selected";
        const updateSelectedSeat = seats[seatIndex].seatName;
        const updateSelectedSeatId = seats[seatIndex].seatId;
        const updatedSelectedSeats = selectedSeats + 1;
        this.setState({
          seats: updatedSeats,
          selectedSeat: updateSelectedSeat,
          selectedSeatId: updateSelectedSeatId,
          selectedSeats: updatedSelectedSeats
        });
      }
    } else if (seats[seatIndex].status === "booked") {
      alert("Sorry, But that seat is already booked !");
    } else {
      const updatedSeats = [...seats];
      updatedSeats[seatIndex].status = "available";
      const updateSelectedSeat = "";
      const updateSelectedSeatId = "";
      const updatedSelectedSeats = 0;
      this.setState({
        seats: updatedSeats,
        selectedSeat: updateSelectedSeat,
        selectedSeatId: updateSelectedSeatId,
        selectedSeats: updatedSelectedSeats
      });
    }
  };

  handleBookTicket = e => {
    e.preventDefault();

    if (!this.state.selectedSeats) {
      alert("Please Select a seat !");
    } else {
      this.setState({ modalShow: true });
    }
  };

  handleUnBookTicket = async e => {
    e.preventDefault();
    let formData = new FormData();
    formData.append("action", "unBookTicket");
    formData.append("bookingId", localStorage.getItem("bookingId"));
    const { data } = await axios.post(apiEndpoint, formData);
    if (data.success) {
      localStorage.removeItem("bookingId");
      localStorage.removeItem("seatName");
      window.location.reload();
    }
  };

  handleChange = async e => {
    const { value, name } = e.target;
    this.setState({ [name]: value });
  };

  handleBookingSubmit = async e => {
    e.preventDefault();
    this.setState({ modalShow: false });
    let formData = new FormData();
    formData.append("action", "bookTicket");
    formData.append("seatId", this.state.selectedSeatId);
    formData.append("customerName", this.state.name);
    formData.append("customerEmail", this.state.email);
    const { data } = await axios.post(apiEndpoint, formData);
    localStorage.setItem("bookingId", data.data);
    localStorage.setItem("seatName", this.state.selectedSeat);
    window.location.reload();
  };

  handleModalClose = e => {
    this.setState({ modalShow: false });
  };

  render() {
    return (
      <React.Fragment>
        {this.state.seats && (
          <SeatSelection
            seats={this.state.seats}
            handleSeatSelect={this.handleSeatSelect}
            handleBookTicket={this.handleBookTicket}
            handleUnBookTicket={this.handleUnBookTicket}
          />
        )}
        {this.state.seats && (
          <UserDetails
            name={this.state.name}
            email={this.state.email}
            selectedSeat={this.state.selectedSeat}
            modalShow={this.state.modalShow}
            handleModalClose={this.handleModalClose}
            handleChange={this.handleChange}
            handleBookingSubmit={this.handleBookingSubmit}
          />
        )}
      </React.Fragment>
    );
  }
}

export default App;
